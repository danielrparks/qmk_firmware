/* Copyright 2020 Daniel Parks
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H
#include "print.h"

//#define SUPERSKIP_DEBUG
#if defined(CONSOLE_ENABLE) && defined(SUPERSKIP_DEBUG)
#define REAL_SUPERSKIP_DEBUG
#define SKD(fn, ...) superskip_debug_ ## fn(__VA_ARGS__)
#else
#define SKD(fn, ...) ((void) 0)
#endif

enum {
	TD_SUPER_SKIP
};

#ifdef REAL_SUPERSKIP_DEBUG
void superskip_debug_count(char *desc, int count) {
	uprintf("Superskip: %s with count %d\n", desc, count);
}

void superskip_debug_send(int sending, char *desc) {
	char* verb;
	switch (sending) {
		case 1:
			verb = "sending";
			break;
		case 2:
			verb = "tapping";
			break;
		default:
			verb = "unsending";
	}
	uprintf("Superskip: %s %s\n", verb, desc);
}

void superskip_debug_end(char *desc) {
	uprintf("Superskip: exiting %s\n", desc);
}
#endif

void superskip_each(qk_tap_dance_state_t *state, void* user_data) {
	SKD(count, "each", state->count);
	if (state->count == 1) {
		SKD(send, 1, "KC_MEDIA_PLAY_PAUSE");
		register_code(KC_MEDIA_PLAY_PAUSE);
	}
	else if (state->count == 2) {
		SKD(send, 0, "KC_MEDIA_PLAY_PAUSE");
		unregister_code(KC_MEDIA_PLAY_PAUSE);
	}
	SKD(end, "each");
}

void superskip_finished(qk_tap_dance_state_t *state, void* user_data) {
	SKD(count, "finished", state->count);
	if (state->count == 1) {
		SKD(send, 0, "KC_MEDIA_PLAY_PAUSE");
		unregister_code(KC_MEDIA_PLAY_PAUSE);
	}
	else if (state->count == 2) {
		SKD(send, 2, "KC_MEDIA_PLAY_PAUSE");
		tap_code_delay(KC_MEDIA_PLAY_PAUSE, 10);
		SKD(send, 2, "KC_MEDIA_NEXT_TRACK");
		tap_code_delay(KC_MEDIA_NEXT_TRACK, 10);
	}
	else if (state->count == 3) {
		SKD(send, 2, "KC_MEDIA_PLAY_PAUSE");
		tap_code_delay(KC_MEDIA_PLAY_PAUSE, 10);
		SKD(send, 2, "KC_MEDIA_PREV_TRACK");
		tap_code_delay(KC_MEDIA_PREV_TRACK, 10);
	}
	SKD(end, "finished");
}

void superskip_reset(qk_tap_dance_state_t *state, void *user_data) {
	SKD(count, "reset", state->count);
	if (state->count == 1) {
		unregister_code(KC_MEDIA_PLAY_PAUSE);
	}
	SKD(end, "reset");
}

qk_tap_dance_action_t tap_dance_actions[] = {
	[TD_SUPER_SKIP] = ACTION_TAP_DANCE_FN_ADVANCED(superskip_each, superskip_finished, superskip_reset)
};

// Defines names for use in layer keycodes and the keymap
enum layer_names {
    _BASE,
		_DVBASE,
    _FN
};

// Defines the keycodes used by our macros in process_record_user
//enum custom_keycodes {
//};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    /* Base */
    [_BASE] = LAYOUT(
				LT(_FN, KC_ESCAPE), KC_F1, KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12, KC_INSERT, KC_DELETE, TD(TD_SUPER_SKIP),
				KC_GRAVE, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_MINUS, KC_EQUAL, KC_BSPACE, KC_AUDIO_VOL_UP,
				KC_TAB, KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_LBRACKET, KC_RBRACKET, KC_BSLASH, KC_AUDIO_VOL_DOWN,
				KC_ESCAPE, KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, KC_SCOLON, KC_QUOTE, KC_ENTER, KC_PSCREEN,
				KC_LSFT, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_N, KC_M, KC_COMMA, KC_DOT, KC_SLASH, KC_RSFT, KC_UP, 0x78, // XF86LaunchA
				KC_LCTL, KC_LGUI, KC_LALT, KC_SPACE, MT(MOD_LALT, KC_RALT), KC_RGUI, KC_RCTL, KC_LEFT, KC_DOWN, KC_RIGHT
    ),
		[_DVBASE] = LAYOUT(
				LT(_FN, KC_ESCAPE), KC_F1, KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12, KC_INSERT, KC_DELETE, TD(TD_SUPER_SKIP),
				KC_GRAVE, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_MINUS, KC_EQUAL, KC_BSPACE, KC_AUDIO_VOL_UP,
				KC_TAB, KC_QUOTE, KC_COMMA, KC_DOT, KC_P, KC_Y, KC_F, KC_G, KC_C, KC_R, KC_L, KC_SLASH, KC_EQUAL, KC_BSLASH, KC_AUDIO_VOL_DOWN,
				KC_ESCAPE, KC_A, KC_O, KC_E, KC_U, KC_I, KC_D, KC_H, KC_T, KC_N, KC_S, KC_MINUS, KC_ENTER, KC_PSCREEN,
				KC_LSFT, KC_SCOLON, KC_Q, KC_J, KC_K, KC_X, KC_B, KC_M, KC_W, KC_V, KC_Z, KC_RSFT, KC_UP, 0x78, // XF86LaunchA
				KC_LCTL, KC_LGUI, KC_LALT, KC_SPACE, MT(MOD_LALT, KC_RALT), KC_RGUI, KC_RCTL, KC_LEFT, KC_DOWN, KC_RIGHT
		),
    [_FN] = LAYOUT(
				KC_NO, KC_F13, KC_F14, KC_F15, KC_F16, KC_F17, KC_F18, KC_F19, KC_F20, KC_F21, KC_F22, KC_F23, KC_F24, KC_TRNS, QK_BOOT, KC_AUDIO_MUTE,
				DYN_REC_STOP, DYN_REC_START1, DYN_MACRO_PLAY1, DYN_REC_START2, DYN_MACRO_PLAY2, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, DF(_BASE), DF(_DVBASE), KC_TRNS, KC_MEDIA_NEXT_TRACK,
				KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_PAUSE, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_SYSREQ, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_MEDIA_PREV_TRACK,
				KC_TRNS, KC_TRNS, KC_TRNS, EEPROM_RESET, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, MAGIC_TOGGLE_NKRO, KC_SYSTEM_SLEEP, KC_TRNS, KC_TRNS, KC_TRNS,
				KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_PAUSE, KC_APPLICATION, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_PGUP, KC_TRNS,
				KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_HOME, KC_PGDOWN, KC_END
    )
};

/*
bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case QMKBEST:
            if (record->event.pressed) {
                // when keycode QMKBEST is pressed
                SEND_STRING("QMK is the best thing ever!");
            } else {
                // when keycode QMKBEST is released
            }
            break;
        case QMKURL:
            if (record->event.pressed) {
                // when keycode QMKURL is pressed
                SEND_STRING("https://qmk.fm/\n");
            } else {
                // when keycode QMKURL is released
            }
            break;
    }
    return true;
}
*/
void keyboard_post_init_user(void) {
  // Customise these values to desired behaviour
  //debug_enable=true;
  //debug_matrix=true;
  //debug_keyboard=true;
  //debug_mouse=true;
}
